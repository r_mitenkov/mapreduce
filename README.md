This project contains Java application that was used in optional homework "Your Map-Reduce"

Dataset is in the "jobsfull.csv" file (it was downloaded from https://www.kaggle.com/PromptCloudHQ/jobs-on-naukricom)
It contains information about job vacancies
Fields to discribe job vacancy in dataset:
      1) company
      2) education	
      3) experience	
      4) industry	      (was used, one vacancy can have some industries)
      5) jobdescription	
      6) jobid	
      7) joblocation_address	
      8) jobtitle	
      9) numberofpositions (was used)	
      10)payrate	
      11)postdate	
      12)site_name	
      13)skills	(was used, one vacancy can require some skills)
      14)uniq_id

Task: To define what industries have more positions to work and what skills are useful for these industries

Output format:
      NameOfIndustry1 - number of positions: X, skills: skill1, skill2, ...
      NameOfIndustry2 - number of positions: X, skills: skill1, skill2, ...
      ...

"jobsshot.csv" file - is a shot version of dataset that contains only fields that were used in analysis. 


### Hadoop installation ###

Hadoop was installed using docker container https://hub.docker.com/r/sequenceiq/hadoop-docker/

command to run container: docker run -it sequenceiq/hadoop-docker:2.7.0 /etc/bootstrap.sh -bash

command to copy files to docker container: docker cp 'path_to_file'  container_id:/

command to copy files to hdfs: $HADOOP_PREFIX/bin/hdfs dfs -put 'path_to_file_in_docker_container'

command to start mapreduce: $HADOOP_PREFIX/bin/hadoop jar  target/MitenkovMapReduce.jar JobsAds jobsshot.csv output

package input;

import java.util.Arrays;

public class JobDescription {
    private String company;
    private Integer numberOfPositions;
    private String[] industry;
    private String skills;

    public JobDescription (String jobDescription){
        String[] jobInfo = jobDescription.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        company = jobInfo[0];
        if (jobInfo[8].equals("\"\"")){
            numberOfPositions = 1;
        }
        else {
            numberOfPositions = Integer.parseInt(jobInfo[8]);
        }
        industry = jobInfo[3].split(" / ");
        skills = jobInfo[12];
    }

    @Override
    public String toString() {
        return company + "\n\t" + numberOfPositions + "\n\t" + Arrays.toString(industry);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getNumberOfPositions() {
        return numberOfPositions;
    }

    public void setNumberOfPositions(Integer numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    public String[] getIndustry() {
        return industry;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public void setIndustry(String[] industry) {
        this.industry = industry;
    }
}

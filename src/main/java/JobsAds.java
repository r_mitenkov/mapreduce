import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import input.JobDescription;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class JobsAds {

    public static class JobsMapper extends Mapper<Object, Text, Text, Text>{

        private final static Text value = new Text();
        private Text industry = new Text();

        public void map(Object key, Text text, Context context
        ) throws IOException, InterruptedException {
            try {
                StringTokenizer iterator = new StringTokenizer(text.toString(), "\n");
                while (iterator.hasMoreTokens()) {
                    Text currentString = new Text();
                    currentString.set(iterator.nextToken());

                    JobDescription jobDescription = new JobDescription(currentString.toString());

                    for(int i = 0; i < jobDescription.getIndustry().length; i++){
                        industry.set(jobDescription.getIndustry()[i]);
                        value.set(jobDescription.getNumberOfPositions().toString() + "__separator__" + jobDescription.getSkills());
                        context.write(industry, value);
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static class JobsReducer extends Reducer<Text,Text,Text,Text> {

        private AtomicInteger result_count = new AtomicInteger(0);
        private Text result = new Text();
        private static String outputNumberOfPositionPrefix = " - number of positions: ";
        private static String outputSkillsPrefix = ", skills: ";

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            Integer count_sum = new Integer(0);
            String val_str;
            Set<String> allSkills = new HashSet<>();

            for (Text val : values) {
                val_str = val.toString();
                if (val_str.contains(outputNumberOfPositionPrefix)){
                    String correct_number_str = val_str.substring(outputNumberOfPositionPrefix.length(),
                            val_str.indexOf(outputSkillsPrefix));
                    count_sum += Integer.parseInt(correct_number_str);
                    String current_skill = val_str.substring(val_str.indexOf(outputSkillsPrefix) + outputSkillsPrefix.length(),
                            val_str.length());
                    if (!allSkills.contains(current_skill) && !current_skill.isEmpty()){
                        allSkills.add(current_skill);
                    }
                }
                else{
                    String[] parts = val_str.split("__separator__");
                    count_sum += Integer.parseInt(parts[0]);
                    String current_skill = parts[1];
                    if (!allSkills.contains(current_skill) && !current_skill.isEmpty()){
                        allSkills.add(current_skill);
                    }
                }
            }
            result_count.set(count_sum);
            String result_string = outputNumberOfPositionPrefix + result_count.toString() + outputSkillsPrefix;
            for (String skill : allSkills){
                result_string += skill + ", ";
            }
            if (!allSkills.isEmpty()){
                result.set(result_string.substring(0, result_string.length() - 2));
            }
            else {
                result.set(result_string);
            }
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "job's ads");
        job.setJarByClass(JobsAds.class);
        job.setMapperClass(JobsMapper.class);
        job.setCombinerClass(JobsReducer.class);
        job.setReducerClass(JobsReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}